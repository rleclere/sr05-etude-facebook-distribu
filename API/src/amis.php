<?php 
include('api/Model/DatabasesConnector.php');
include('api/Model/AmisModel.php');
include('api/Model/utilisateurModel.php');
include('header.php'); 

$db = new DatabaseConnector();
$AmisModel = new AmisModel($db->getConnection());
$utilisateurModel = new utilisateurModel($db->getConnection());
$Amis = $AmisModel->findAll();
$uM = $utilisateurModel->find();
?>
<h2>Page des amis</h2>
<div class="container" style="padding:10px">
    <!-- Recherche -->
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <button class="btn btn-outline-primary" type="button" id="button-addon1" >Ajouter</button>
            <select class="form-control" id="typeHttp">
                <option value="http://">http://</option>
                <option value="https://">https://</option>
            </select>
        </div>
            <input type="search" class="form-control" id="url" placeholder="Url" aria-describedby="button-addon1">
    </div>
    <div id="popupListAmi" style="display:none"></div>
    <?php foreach ($Amis as $Ami) { ?>

<!-- Boucle -->
<div class="card-deck" >

    <!-- Affichage offre -->
    <div class="card shadow p-1 mb-3 bg-white rounded ">
        <div class="col-lg-12 col-md-12 col-sm-8 d-flex">
            <div class="card-body d-flex flex-column">
                <p class="card-text pb-">
                    <!-- Titre -->
                    <h3>
                        <span class="blocAmi" id="Ami-<?php echo $Ami["id"]; ?>"><?php echo $Ami["nom"]." ".$Ami["prenom"] ?> : <span><?php echo $Ami["url"] ?></span></span>
                        
                        <?php 
                               if ($Ami["type"] == "2") { 
                            ?>  
                                <span class="btn btn-success mx-auto ">Ami(e)</span>
                            <?php
                            } else if ($Ami["type"] == "1"){
                            ?>
                                <span class="btn btn-info mx-auto">Demande envoyée</span>
                            <?php
                            } else if ($Ami["type"] == "3"){
                            ?>
                                <span class="btn btn-info mx-auto">Stranger</span>
                            <?php
                            } else if ($Ami["type"] == "0"){
                            ?>
                                <span class="btn btn-info mx-auto">Demande reçue</span>
                            <?php
                            }
                         ?>
                    </h3>
                </p>
            

                <div>
                    <div class="float-right">
                        <?php
                            if ($Ami["type"] == "0") { 
                            ?>  
                                <span class="btn btn-success mx-auto vDA"><input type="hidden" value="<?php echo $Ami["id"]; ?>">Valider la demande d'ami </span>
                                <span class="btn btn-danger mx-auto rDA"><input type="hidden" value="<?php echo $Ami["id"]; ?>">Refuser la demande d'ami </span>
                            <?php
                            } else if ($Ami["type"] == "1") { 
                            ?>
                                <span class="btn btn-danger mx-auto rDA"><input type="hidden" value="<?php echo $Ami["id"]; ?>">Annuler la demande</span>
                            <?php
                            } else if ($Ami["type"] == "2") { 
                            ?>
                                <span class="btn btn-info mx-auto demandeListAmi"><input type="hidden" value="<?php echo $Ami["id"]; ?>">Voir la liste d'amis</span> 
                                <span class="btn btn-danger mx-auto rDA"><input type="hidden" value="<?php echo $Ami["id"]; ?>">Supprimer</span>
                            <?php
                            } else if ($Ami["type"] == "3") { 
                            ?> 
                                <span class="btn btn-success mx-auto sDA"><input type="hidden" value="<?php echo $Ami["url"]; ?>">Envoyer une demande</span>
                            <?php
                            } 
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                        <?php } ?>

<script>
$(document).ready(function() {  

            $(".demandeListAmi").click(function(e) {
                id = $(this).children("input").val();
                urlValidation = $("#Ami-" + id).children("span").text();
                $.ajax({
                            method: "POST",
                            url: urlValidation+"/api/Amis.php?demandeListAmi=<?php echo $uM[0]['url']; ?>",
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                            success: function(data) { afficherListAmi(urlValidation,data); }
                })
                .fail(function(msg) {
                    console.log("Error: " + JSON.stringify(msg));
                })
            });

            $(".rDA").click(function(e) {
                id = $(this).children("input").val();
                urlValidation = $("#Ami-" + id).children("span").text();
                $.ajax({
                        method: "DELETE",
                        url: "/api/Amis.php?id="+id,
                        contentType: 'application/json; charset=utf-8',
                        dataType: "text",
                    })
                    .done(function(msg) {
                        alert("Delete effectuer");
                        $.ajax({
                            method: "PUT",
                            url: urlValidation+"/api/Amis.php?reponse=3&origin=<?php echo $uM[0]['url']; ?>",
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                        })
                        reload(250);
                    })
                    .fail(function(msg) {
                        console.log("Error: " + JSON.stringify(msg));
                    })
            });

            $("#button-addon1, .sDA").click(function(e) {
                let met;
                if($(this).attr('id') == "button-addon1") {
                    urlbase = $("#typeHttp").val() + $("#url").val();
                    met = "POST"
                }
                else {
                    met = "PUT"
                    urlbase = $(this).children("input").val();
                }
                var data = {
                    nom: "<?php echo $uM[0]['nom']; ?>",
                    prenom: "<?php echo $uM[0]['prenom']; ?>",
                    url: "<?php echo $uM[0]['url']; ?>",
                    type: 0
                }
                $.ajax({
                        method: met,
                        url: urlbase+"/api/Amis.php",
                        contentType: 'application/json; charset=utf-8',
                        dataType: "text",
                        data: JSON.stringify(data),
                    })
                    .done(function(msg) {
                        alert("Add");
                        // Ajouter validation
                        $.ajax({
                            method: met,
                            url: "/api/Amis.php?urlAdd="+urlbase,
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                        })
                        reload(250);
                    })
                    .fail(function(msg) {
                        console.log("Error: " + JSON.stringify(msg));
                    })
            });

            $(".vDA").click(function(e) {
                idValidation = $(this).children("input").val();
                urlValidation = $("#Ami-" + idValidation).children("span").text();
                var data = {
                    nom: "<?php echo $uM[0]['nom']; ?>",
                    prenom: "<?php echo $uM[0]['prenom']; ?>",
                    url: "<?php echo $uM[0]['url']; ?>",
                    type: 0
                }
                $.ajax({
                        method: "PUT",
                        url: "/api/Amis.php?idvDA="+idValidation,
                        contentType: 'application/json; charset=utf-8',
                        dataType: "text",
                    })
                    .done(function(msg) {
                        alert("Demande d'ami validé");
                        $.ajax({
                            method: "PUT",
                            url: urlValidation+"/api/Amis.php?reponse=2&origin=<?php echo $uM[0]['url']; ?>",
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                            data: JSON.stringify(data),
                        })
                        reload(250);
                    })
                    .fail(function(msg) {
                        console.log("Error: " + JSON.stringify(msg));
                    })
            });

            function reload(time){
                setTimeout(function(){ location.reload(); },time);
            }

            function afficherListAmi(urlAmi, data) {
                data = JSON.parse(data);
                $("#popupListAmi").empty();
                $("#popupListAmi").append('<h4 style="text-align:center">Liste des amis de : ' + urlAmi + ' <span class="btn btn-danger mx-auto" onclick="$(\'#popupListAmi\').slideUp(600)">Fermer</span></h4>');
                for(let i = 0; i < data.length; i++) {
                    bloc = ''
                    bloc += '<div class="card-deck" >'
                    bloc += '<div class="card shadow p-1 mb-3 bg-white rounded ">'
                    bloc += '<div class="col-lg-12 col-md-12 col-sm-8 d-flex">'
                    bloc += '<div class="card-body d-flex flex-column">'
                    bloc += '<p class="card-text pb-">'
                    bloc += '<h3>'
                    if(data[i]["url"] != "<?php echo $uM[0]["url"] ?>") {
                        bloc += '<span class="blocAmi">'+data[i]["nom"]+' '+data[i]["prenom"]+' : <span>'+data[i]["url"]+'</span> <span class="btn btn-success mx-auto ">Ami(e) d\'ami(e)</span></span>'
                    }
                    else {
                        bloc += '<span class="blocAmi">'+data[i]["nom"]+' '+data[i]["prenom"]+' : <span>'+data[i]["url"]+'</span> <span class="btn btn-dark mx-auto ">Moi</span></span>'
                    }
                    bloc += '</h3>'
                    bloc += '</p>'
                    bloc += '</div>'
                    bloc += '</div>'
                    bloc += '</div>'
                    bloc += '</div>'
                    $("#popupListAmi").append(bloc);

                    if(data[i]["url"] != "<?php echo $uM[0]["url"] ?>") {
                        var dataNewStranger = {
                            nom: data[i]['nom'],
                            prenom: data[i]['prenom'],
                            url: data[i]['url'],
                            type: 3
                        }

                        var mineData = {
                            nom: "<?php echo $uM[0]['nom']; ?>",
                            prenom: "<?php echo $uM[0]['prenom']; ?>",
                            url: "<?php echo $uM[0]['url']; ?>",
                            type: 3
                        }

                        $.ajax({
                            method: "POST",
                            url: "/api/Amis.php?strangerAdd="+data[i]["url"],
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                            data: JSON.stringify(dataNewStranger),
                        })

                        $.ajax({
                            method: "POST",
                            url: data[i]["url"]+"/api/Amis.php?strangerAdd=<?php echo $uM[0]["url"] ?>",
                            contentType: 'application/json; charset=utf-8',
                            dataType: "text",
                            data: JSON.stringify(mineData),
                        })
                    }
                }
                $("#popupListAmi").append('<h4 style="text-align:center">Votre liste d\'amis : </h4>')
                $("#popupListAmi").slideDown(600);
            }
});
</script>
<?php include('footer.php'); ?>
