<?php
include 'Model/DatabasesConnector.php';
include 'Model/AmisModel.php';
include 'Model/UserModel.php';
include 'Controller/AmisController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (array_key_exists('HTTP_HOST', $_SERVER)) {
    $origin = $_SERVER['HTTP_HOST'];
} else if (array_key_exists('SERVER_NAME', $_SERVER)) {
    $origin = $_SERVER['SERVER_NAME'];
} else {
    $origin = $_SERVER['REMOTE_ADDR'];
}

// the user id is, of course, optional and must be a number:
$userId = null;
if (isset($_GET['id'])) {
    $userId = (int) $_GET['id'];
}

$dbConnection = new DatabaseConnector();

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and user ID to the PersonController and process the HTTP request:
$controller = new AmisController($dbConnection->getConnection(), $requestMethod, $userId,$origin);
$controller->processRequest();