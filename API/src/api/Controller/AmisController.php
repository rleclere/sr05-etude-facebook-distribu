<?php

class AmisController {

    private $db;
    private $requestMethod;
    private $userId;

    private $AmisModel;

    private $UserModel;

    public function __construct($db, $requestMethod, $userId, $origin)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->userId = $userId;

        $this->AmisModel = new AmisModel($db);

        $this->UserModel = new UserModel($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->userId) {
                    $response = $this->getUser($this->userId);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            case 'POST':
                if(isset($_GET["urlAdd"]) && $_GET["urlAdd"] != $this->UserModel->getUser()["url"]) {
                    $response = $this->createUserFromDemandeSend($_GET["urlAdd"]);
                }
                else if(isset($_GET["demandeListAmi"])) {
                    $response = $this->sendFriendsList($_GET["demandeListAmi"]);
                }
                else if(isset($_GET["strangerAdd"])) {
                    $response = $this->addStranger($_GET["strangerAdd"]);
                }
                else {
                    $response = $this->createUserFromRequest();
                }
                break;
            case 'PUT':
                if(isset($_GET["idvDA"])){ // Validation de la demande d'ami
                    $response = $this->updateDemandeToAmi($_GET["idvDA"]);
                }
                else if(isset($_GET["origin"]) && isset($_GET["reponse"])){
                    if($_GET["reponse"] == 2) {
                        $response = $this->retourDemande($_GET["origin"],2);
                    }
                    else if($_GET["reponse"] == 3) {
                        $response = $this->retourDemande($_GET["origin"],3);
                    }
                }
                else if(isset($_GET["urlAdd"])) {
                    $response = $this->retourDemande($_GET["urlAdd"],1);
                }
                else {
                    $input = (array) json_decode(file_get_contents('php://input'), TRUE);
                    $response = $this->retourDemande($input["url"],0);
                }
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->userId);
                break;
            case 'OPTIONS':
                $response = $this->c200();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllUsers()
    {
        $result = $this->AmisModel->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getUser($id)
    {
        $result = $this->AmisModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createUserFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAmis($input) || $this->UserModel->getUser()["url"] == $input["url"]) {
            return $this->unprocessableEntityResponse();
        }
        $this->AmisModel->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function createUserFromDemandeSend($url)
    {
        $input["url"] = $url;
        $input["type"] = 1;
        $input["nom"] = "";
        $input["prenom"] = "";
        if (! $this->validateAmis($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->AmisModel->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function addStranger($url)
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAmis($input) || $this->UserModel->getUser()["url"] == $input["url"]) {
            return $this->unprocessableEntityResponse();
        }
        $this->AmisModel->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateAmisFromRequest($id)
    {
        $result = $this->AmisModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAmis($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->AmisModel->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function updateDemandeToAmi($id)
    {
        $result = $this->AmisModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->AmisModel->updateTypeByID($id,2);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function retourDemande($url,$type)
    {
        $result = $this->AmisModel->findUrl($url);
        if (! $result) {
            return $this->notFoundResponse();
        }
        if($type == 2) {
            $input = (array) json_decode(file_get_contents('php://input'), TRUE);
            $this->AmisModel->update(intval($result[0]["id"]), $input);
        }
        $this->AmisModel->updateTypeByURL($url,$type);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }


    private function deleteUser($id)
    {
        $result = $this->AmisModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->AmisModel->updateTypeByID($id,3);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateAmis($input)
    {
        if (! isset($input['url'])) {
            return false;
        }
        if (! isset($input['nom'])) {
            return false;
        }
        if (! isset($input['prenom'])) {
            return false;
        }
        return true;
    }

    private function sendFriendsList($to){
         $result = $this->AmisModel->findUrl($to);
        if (! $result) {
            return $this->notFoundResponse();
        }
        echo json_encode($this->AmisModel->findFriends());
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function c200(){
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}