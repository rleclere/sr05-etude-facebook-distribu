<?php

class MessagesController {

    private $db;
    private $requestMethod;
    private $messagesId;
    private $messageMode;

    private $MessagesModel;

    public function __construct($db, $requestMethod, $messagesId, $messageMode)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->messagesId = $messagesId;
        $this->messageMode = $messageMode;

        $this->MessagesModel = new MessagesModel($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) { 
            case 'GET':
                if($this->messageMode) {
                    if ($this->messageMode === "one" && $this->messagesId) {
                        $response = $this->getMessages($this->messagesId);
                    } else if ($this->messageMode === "mine") {
                        $response = $this->getAllMyMessages();
                    }
                } else {
                    $response = $this->getAllMessages();
                };
                break;
            case 'POST':
                $response = $this->createMessagesFromRequest();
                break;
            case 'PUT':
                $response = $this->updateMessagesFromRequest($this->messagesId);
                break;
            case 'DELETE':
                $response = $this->deleteMessages($this->messagesId);
                break;
            case 'OPTIONS':
                $response = $this->c200();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllMessages()
    {
        $result = $this->MessagesModel->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getAllMyMessages()
    {
        $result = $this->MessagesModel->findMine();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getMessages($id)
    {
        $result = $this->MessagesModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createMessagesFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateMessages($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->MessagesModel->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateMessagesFromRequest($id)
    {
        $result = $this->MessagesModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateMessages($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->MessagesModel->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteMessages($id)
    {
        $result = $this->MessagesModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->MessagesModel->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateMessages($input)
    {
        if (! isset($input['auteur'])) {
            return false;
        }
        if (! isset($input['emetteur'])) {
            return false;
        }
        if (! isset($input['destinataire'])) {
            return false;
        }
        if (! isset($input['contenue'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }

    private function c200(){
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }
}