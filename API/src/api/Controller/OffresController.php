<?php

class OffresController {

    private $db;
    private $requestMethod;
    private $offresId;

    private $OffresModel;

    public function __construct($db, $requestMethod, $offresId)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->offresId = $offresId;
        $this->OffresModel = new OffresModel($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) { 
            case 'GET':
                $response = $this->getAllOffres();
                break;
            case 'POST':
                if(isset($_POST["idToDL"])){
                    $response = $this->deleteOffres($_POST["idToDL"]);
                }else{
                    $response = $this->createOffresFromRequest();
                }
                header("Location: ../../offres.php");
                exit;
                break;
            case 'OPTIONS':
                $response = $this->c200();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }


    private function getAllOffres()
    {
        $result = $this->OffresModel->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }


    private function createOffresFromRequest()
    {
        if(isset($_POST["titre"]) && isset($_POST["description"])){
            $input = (array) [$_POST["titre"],$_POST["description"]];
            $this->OffresModel->insert($input);
            $response['status_code_header'] = 'HTTP/1.1 201 Created';
            $response['body'] = null;
            return $response;
        }
        else
            return $this->unprocessableEntityResponse();
    }

    private function deleteOffres($id)
    {
        $result = $this->OffresModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->OffresModel->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateOffres($input)
    {
        if (! isset($input['titre'])) {
            return false;
        }
        if (! isset($input['description'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }

    private function c200(){
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }
}