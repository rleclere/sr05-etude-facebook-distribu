<?php

class RecherchesController {

    private $db;
    private $requestMethod;
    private $userId;

    private $AmisModel;
    private $UserModel;
    private $RecherchesModel;

    public function __construct($db, $requestMethod, $userId, $origin)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->userId = $userId;

        $this->AmisModel = new AmisModel($db);

        $this->UserModel = new UserModel($db);

        $this->RecherchesModel = new RecherchesModel($db);

        $this->OffresModel = new OffresModel($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'POST':
                if(isset($_GET["init"])){
                    $this->initDemande();
                }
                else {
                    $this->rechercheProduit();
                }
                $response = $this->c200();
                break;
            case 'OPTIONS':
                $response = $this->c200();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function c200(){
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }


    private function rechercheProduit(){
        $me = $this->UserModel->getUser();
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if($me["url"] != $input["demandeur"]) {
            $result = $this->RecherchesModel->findUrl($input["demandeur"]);
            if (!isset($result)) {
                $this->RecherchesModel->insert($input["demandeur"]);
                $result[0] = array("demandeur" => $input["demandeur"], "etat" => "0");
            }
            if($result[0]["etat"] === "0") {
                $this->RecherchesModel->update($input["demandeur"],1);
                $temp = $this->OffresModel->findByTitre($input["produit"]);
                $annonces = [];
                if(!empty($temp)) {
                    $temp[0]["nom"] = $me["nom"];
                    $temp[0]["prenom"] = $me["prenom"];
                    $annonces = array($me["url"] => $temp[0]);
                }
                $amis = $this->AmisModel->findAllWithout($input["parent"]);
                $input["parent"] = $me["url"];
                foreach($amis as $ami){
                    if(($annonceAmi = $this->curlSend($ami["url"],"/api/Recherches.php",$input)) != NULL){
                            foreach(json_decode($annonceAmi) as $key => $val){
                                $annonces[$key] = $val;
                            }

                    }
                }
                echo json_encode($annonces);
            }
        }
    }

    private function curlSend($url,$page,$content = null){
        preg_match_all("/https?:\/\/(.*)(\/.*)?/m", $url, $matches);
        $urlCURL = $url.$page;
        $resolve = [$matches[1][0].":80:172.18.0.2"];
        $ch = curl_init();
        # Setup request to send json via POST.
        curl_setopt($ch,CURLOPT_URL,$urlCURL);
        curl_setopt($ch,CURLOPT_RESOLVE, $resolve);
        if($content != null){
            $payload = json_encode($content);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Content-Length:'.strlen($payload)));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload );
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        # Send request.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        curl_close($ch);
        return $result;
    }

    private function initDemande(){
        $me = $this->UserModel->getUser();
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if($me["url"] != $input["demandeur"]) {
            $result = $this->RecherchesModel->findUrl($input["demandeur"]);
            $amis = $this->AmisModel->findAllWithout($input["demandeur"]);
            $input["parent"] = $me["url"];
            if (empty($result)) {
                $this->RecherchesModel->insert($input["demandeur"]);
                foreach($amis as $ami){
                        $retour = $this->curlSend($ami["url"],"/api/Recherches.php?init",$input);
                }
            }
            else {
                if($result[0]["etat"] == "1") {
                    $this->RecherchesModel->update($input["demandeur"],0);
                    foreach($amis as $ami){
                            $retour = $this->curlSend($ami["url"],"/api/Recherches.php?init",$input);
                    }
                }
            }
        }
    }
    
}