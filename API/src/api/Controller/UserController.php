<?php

class UserController {

    private $db;
    private $requestMethod;
    private $value;

    private $UserModel;

    public function __construct($db, $requestMethod, $value)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->value = $value;

        $this->UserModel = new UserModel($db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->value) {
                    $response = $this->getUser($this->value);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            case 'POST':
                $response = $this->createUserFromRequest();
                break;
            case 'PUT':
                $response = $this->updateConfidentiality();
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->value);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function getAllUsers()
    {
        $result = $this->UserModel->findAll();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function getUser($id)
    {
        $result = $this->UserModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createUserFromRequest()
    {
        $input = (array) json_decode(file_get_contents('php://input'), TRUE);
        if (! $this->validateAmis($input)) {
            return $this->unprocessableEntityResponse();
        }
        $this->UserModel->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['body'] = null;
        return $response;
    }

    private function updateConfidentiality()
    {
        $result = $this->UserModel->getUser();
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->UserModel->update($this->value["value"]);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function deleteUser($id)
    {
        $result = $this->AmisModel->find($id);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $this->UserModel->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = null;
        return $response;
    }

    private function validateAmis($input)
    {
        if (! isset($input['url'])) {
            return false;
        }
        if (! isset($input['nom'])) {
            return false;
        }
        if (! isset($input['prenom'])) {
            return false;
        }
        return true;
    }

    private function unprocessableEntityResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = json_encode([
            'error' => 'Invalid input'
        ]);
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}