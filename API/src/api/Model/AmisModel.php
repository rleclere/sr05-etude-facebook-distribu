<?php

class AmisModel {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function authorized($url)
    {
        $statement = "
            SELECT 
                id
            FROM
                amis
            WHERE
                url LIKE :url;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('url' => $url));
            return ($statement->rowCount() > 0);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findAll()
    {
        $statement = "
            SELECT 
                id, url, nom, prenom, type
            FROM
                amis
            ORDER BY type;    
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findAllWithout($url)
    {
        $statement = "
            SELECT 
                id, url, nom, prenom, type
            FROM
                amis
            WHERE url NOT LIKE '%$url%'
            ORDER BY type;    
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findFriends()
    {
        $statement = "
            SELECT 
                id, url, nom, prenom, type
            FROM
                amis
            WHERE type = 2
            ORDER BY type;    
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


    public function find($id)
    {
        $statement = "
            SELECT 
                url, nom, prenom, type
            FROM
                amis
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function findUrl($url)
    {
        $statement = "
            SELECT 
                id, url, nom, prenom, type
            FROM
                amis
            WHERE url = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($url));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO amis 
                (url, nom, prenom, type)
            VALUES
                (:url, :nom, :prenom, :type);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'url' => $input['url'],
                'nom'  => $input['nom'],
                'prenom' => $input['prenom'],
                'type' => $input['type']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function update($id, Array $input)
    {
        $statement = "
            UPDATE amis
            SET 
                url = :url,
                nom  = :nom,
                prenom = :prenom
            WHERE id = :id;
        ";
        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'url' => $input['url'],
                'nom'  => $input['nom'],
                'prenom' => $input['prenom'] // ?? null
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function updateTypeByID($id,$type)
    {
        $statement = "
            UPDATE amis
            SET 
                type = :type
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'type' => $type
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function updateTypeByURL($url,$type)
    {
        $statement = "
            UPDATE amis
            SET 
                type = :type
            WHERE url = :url;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'url' => $url,
                'type' => $type
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM amis
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
}