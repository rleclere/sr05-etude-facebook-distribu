<?php

class DatabaseConnector {

    private $dbConnection = null;

    public function __construct()
    {
    	$file = parse_ini_file("config.ini.php");
        $host = $file["host"];
        $port = $file["port"];
        $db   = $file["db"];
        $user = $file["user"];
        $pass = $file["pass"];

        try {
            $this->dbConnection = new \PDO(
                "mysql:host=$host;port=$port;charset=utf8mb4;dbname=$db",
                $user,
                $pass
            );
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}