<?php

class MessagesModel {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll()
    {
        $statement = "
            SELECT 
                id, auteur, emetteur, destinataire, contenue
            FROM
                messages;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findFromFriends()
    {
         $statement = "
            SELECT 
                id, auteur, emetteur, destinataire, contenue
            FROM
                messages
            WHERE emetteur<> (SELECT url FROM utilisateur LIMIT 1);
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "
            SELECT 
                id, auteur, emetteur, destinataire, contenue
            FROM
                messages
            WHERE id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function findMine()
    {
        $statement = "
            SELECT 
                id, auteur, emetteur, destinataire, contenue
            FROM
                messages
            WHERE auteur= (SELECT CONCAT(nom, ' ', prenom) FROM utilisateur LIMIT 1);

        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function findMineSent()
    {
        $statement = "
            SELECT 
                id, auteur, emetteur, destinataire, contenue
            FROM
                messages
            WHERE emetteur= (SELECT url FROM utilisateur LIMIT 1)  ORDER BY id DESC;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function insert(Array $input)
    {
        $statement = "
            INSERT INTO messages 
                (auteur, emetteur, destinataire, contenue)
            VALUES
                (:auteur, :emetteur, :destinataire, :contenue);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'auteur' => $input['auteur'],
                'emetteur'  => $input['emetteur'],
                'destinataire'  => $input['destinataire'],
                'contenue'  => $input['contenue']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function update($id, Array $input)
    {
        $statement = "
            UPDATE messages
            SET 
                auteur = :auteur,
                emetteur  = :emetteur,
                destinataire = :destinataire,
                contenue = :contenue
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'auteur' => $input['auteur'],
                'emetteur'  => $input['emetteur'],
                'destinataire' => $input['destinataire'],
                'contenue' => $input['contenue']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function delete($id)
    {
        $statement = "
            DELETE FROM messages
            WHERE id = :id;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
}