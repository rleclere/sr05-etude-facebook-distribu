<?php

class RecherchesModel {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findUrl($url)
    {
        $statement = "
            SELECT 
                *
            FROM
                recherches
            WHERE url = :url;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'url' => $url
            ));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function insert($url)
    {
        $statement = "
            INSERT INTO recherches
                (url, etat)
            VALUES
                (:url, :etat);
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'url' => $url,
                'etat'  => 0
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

    public function update($url, $etat)
    {
        $statement = "
            UPDATE recherches
            SET 
                etat = :etat
            WHERE url = :url;
        ";
        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'etat' => (int) $etat,
                'url' => $url
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }
}