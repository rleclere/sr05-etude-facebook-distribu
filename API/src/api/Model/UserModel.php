<?php

class UserModel {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }


    public function getUser()
    {
        $statement = "
            SELECT 
                url, nom, prenom, modePartage
            FROM
                utilisateur;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetch(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


    public function update($num)
    {
        $statement = "
            UPDATE utilisateur
            SET 
                modePartage=:num
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'num' => (int) $num
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }    
    }

}