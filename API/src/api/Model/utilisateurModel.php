<?php

class utilisateurModel {

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function find()
    {
        $statement = "
            SELECT 
                url, nom, prenom, modePartage
            FROM
                utilisateur;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


}