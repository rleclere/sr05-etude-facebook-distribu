<?php
include 'Model/DatabasesConnector.php';
include 'Model/OffresModel.php';
include 'Controller/OffresController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


if (array_key_exists('HTTP_REFERER', $_SERVER)) {
    $origin = $_SERVER['HTTP_REFERER'];
} else if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $origin = $_SERVER['HTTP_ORIGIN'];
} else {
    $origin = $_SERVER['REMOTE_ADDR'];
}
preg_match('%\/\/(.*)\/%',
            $origin, $matches);
$origin = $matches[1];


// the messages id is, of course, optional and must be a number:
$offresId = null;
if (isset($_GET['id'])) {
    $messagesId = (int) $_GET['id'];
}

$dbConnection = new DatabaseConnector();

//$AmisModel->authorized($origin);

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and messages ID to the PersonController and process the HTTP request:
$controller = new OffresController($dbConnection->getConnection(), $requestMethod, $offresId);
$controller->processRequest();