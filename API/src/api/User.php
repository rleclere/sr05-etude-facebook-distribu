<?php
include 'Model/DatabasesConnector.php';
include 'Model/UserModel.php';
include 'Controller/UserController.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// new value of the confidentiality choice
$value = null;
$value = (array) json_decode(file_get_contents('php://input'), TRUE);

$dbConnection = new DatabaseConnector();

$requestMethod = $_SERVER["REQUEST_METHOD"];

// pass the request method and user ID to the PersonController and process the HTTP request:
$controller = new UserController($dbConnection->getConnection(), $requestMethod, $value);
$controller->processRequest();