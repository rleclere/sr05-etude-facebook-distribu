<?php

function curlSend($url,$page,$content = null){
    preg_match_all("/https?:\/\/(.*)(\/.*)?/m", $url, $matches);
    $urlCURL = $url.$page;
    $resolve = [$matches[1][0].":80:172.18.0.2"];
    $ch = curl_init();
    # Setup request to send json via POST.
    curl_setopt($ch,CURLOPT_URL,$urlCURL);
    curl_setopt($ch,CURLOPT_RESOLVE, $resolve);
    if($content != null){
        $payload = json_encode($content);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Content-Length:'.strlen($payload)));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt($ch, CURLOPT_POST, 1);
    }
    # Send request.
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    curl_close($ch);
    return $result;
    
}

echo curlSend("http://site.web","/test.php",array("bonjour" => "test"));