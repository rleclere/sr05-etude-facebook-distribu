<html lang="fr">

<head>
        <title>New Facebook</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdn.bootcss.com/simplePagination.js/1.6/jquery.simplePagination.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

        <!-- STYLE -->
        <link href="/styles/css.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
        <link rel="stylesheet" href="https://cdn.bootcss.com/simplePagination.js/1.6/simplePagination.min.css">
</head>

<body>

        <!-- NAVBAR -->
        <nav class="navbar navbar-expand-lg navbar-light">
                <!-- NAVBAR TOGGLER MOBILE -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                </button>

                <!-- CONTENU NAVBAR -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- PAGES -->
                        <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                        <a class="nav-link" href="/messages.php">Message</a>
                                </li>
                                <li class="nav-item">
                                        <a class="nav-link" href="/amis.php">Amis</a>
                                </li>
                                <li class="nav-item">
                                        <a class="nav-link" href="/parametres.php">Paramètres</a>
                                </li>
                                 <li class="nav-item">
                                        <a class="nav-link" href="/mur.php">Mur</a>
                                </li>
                                <li class="nav-item">
                                        <a class="nav-link" href="/recherches.php">Recherche d'annonces</a>   
                                </li>
                                <li class="nav-item">     
                                        <a class="nav-link" href="/offres.php">Annonces</a>

                                </li>
                        </ul>

                        <!-- ICONE UTILISATEUR -->
                        <span class="fas fa-user pr-3" style="color: white;">
                        </span>

                        
                        <!-- AFFICHAGE CONNECTE -->
                        <span class="float-right" style="color: white;">
                                <?php echo "Pseudo" ?>
                        </span>
                </div>
        </nav>