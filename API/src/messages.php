<?php 
include('header.php'); 
include('api/Model/DatabasesConnector.php');
include('api/Model/MessagesModel.php');
include('api/Model/utilisateurModel.php');
include('api/Model/AmisModel.php');
$db = new DatabaseConnector();
$messageModel = new MessagesModel($db->getConnection());
$ListeMessages = $messageModel->findFromFriends();
$AmisModel = new AmisModel($db->getConnection());
$ListeAmis = $AmisModel->findFriends();
$utilisateurModel = new utilisateurModel($db->getConnection());
$moi = $utilisateurModel->find()[0];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Page de messagerie</title>
</head>
<body>
	<h2 class="mb-4 mt-3 ml-5">Liste des messages recus</h2>
	<div class="card mx-auto pb-0" style="width: 80rem">
  		<div class="card-body">
			<table class="table table-striped table-bordered mb-0">
				<thead class="thead-dark">
					<tr>
						<th>ID</th>
						<th>auteur</th>
						<th>emetteur</th>
						<th>destinataire</th>
						<th>contenu</th>
						<th>Partage</th>
					</tr>
				</thead>
				<tbody>
					<?php
					//var_dump($ListeMessages);
					$i = 0;
					foreach ($ListeMessages as $message) {
						echo '<tr>';
						echo '<td>'.$message["id"].'</td>';
						echo '<td>'.$message["auteur"].'</td>';
						echo '<td>'.$message["emetteur"].'</td>';
						echo '<td>'.$message["destinataire"].'</td>';
						echo '<td>'.$message["contenue"].'</td>';
						echo "<td>
								<button class='btn btn-primary' type='button' id='boutonPartage".$i."' msgId='".$i."'>Partager</button>
								<span class='badge badge-pill badge-success' id='bdgsucces".$i."'>Success</span>
							</td>";
						echo '</tr>';
						$i++;
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
	<h2 class="mb-4 mt-3 ml-5"id="envoi">Espace envoi de messages</h2>
	<div class="form-group text-center">
		<textarea class="form-control mx-auto" style="width: 40rem" name="messageSaisi" id="messageSaisi" placeholder="Saisir votre message ici"></textarea>
		<input type="button" value="Envoyer" class='btn btn-primary center-block' id="boutonEnvoi">
		<span class='badge badge-pill badge-success' id='bdgsuccesEnvoi'>Success</span>
	</div>

<script>
//Initialisation du tableau des amis
    var friendTab = new Array();
    <?php foreach ($ListeAmis as $Ami) { ?>
        var obj = {
            id: "<?php echo $Ami["id"]?>",
            url: "<?php echo $Ami["url"]?>",
            nom: "<?php echo $Ami["nom"]?>",
            prenom: "<?php echo $Ami["prenom"]?>",
            type: "<?php echo $Ami["type"]?>"
        };
        friendTab.push(obj);
    <?php } ?>
//Non-visbilité des badges de succès
$("[id^=bdgsucces]").hide();
//Fonction d'initialisation des fonctions attachées aux boutons
	$(document).ready(function() {
	        //Bouton d'envoi d'un nouveau message
	        $("#boutonEnvoi").click(function(e){
	        	var contenu = $("#messageSaisi").val();
	            console.log(contenu);
	            if(contenu != ""){
	            	//Récupération des données du message envoyé
	            	var auteur = "<?php echo $moi["url"]?>";
	            	var emetteur = "<?php echo $moi["url"]?>";
	            	var destinataire = "Amis";
	            	var data = {
	                    auteur: auteur,
	                    emetteur: emetteur,
	                    destinataire: destinataire,
	                    contenue: contenu
	            	}
	            	            	console.log(data);
	            	//Sauvegarde au sein de la base de données locale
	            	$.ajax({
	                    method: "POST",
	                    url: "/api/Messages.php",
	                    dataType: "text",
	                    contentType: 'application/json; charset=utf-8',
	                    data: JSON.stringify(data)
	                })
	                .done(function(msg) {
	                    //alert("Data Saved: " + msg);
	                })
	                .fail(function(msg) {
	                    console.log("Error: " + msg);
	                })
	                //Envoi à tous les amis
	                $(friendTab).each(function (index, item){
	                	data.destinataire = item.url; //Destinataire de chaque message 
	                	$.ajax({
	                        method: "POST",
	                        url: item.url+"/api/Messages.php",
	                        dataType: "text",
	                        contentType: 'application/json; charset=utf-8',
	                        data: JSON.stringify(data)
	                    })
	                    .done(function(msg) {
	                        //alert("Data Saved: " + data);
	                       
	                    })
	                    .fail(function(msg) {
	                        console.log("Error: " + msg);
	                    })
	                });
	                $("#bdgsuccesEnvoi").show().delay(2000).fadeOut();
	            }
	        });

		$("[id^=boutonPartage]").click(gestionPartage);
	});//fin ready

//Bouton de partage
function gestionPartage(e){
	var clicked_id = $(e.target).attr("msgId");
	let ListeMessages = <?php echo json_encode($ListeMessages);?>;
	let mContenu = ListeMessages[clicked_id]["contenue"];
    console.log(mContenu);
    if(mContenu != null){
        	let auteur = ListeMessages[clicked_id]["auteur"];
        	let emetteur = "<?php echo $moi["url"]?>";
        	let destinataire = "Amis";
        	var data = {
                auteur: auteur,
                emetteur: emetteur,
                destinataire: destinataire,
                contenue: mContenu
        	}
        	console.log(data);
        	//Sauvegarde au sein de la base de données locale
        	$.ajax({
                method: "POST",
                url: "/api/Messages.php",
                dataType: "text",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(data)
            })
            .done(function(msg) {
                alert("Data Saved: " + msg);
            })
            .fail(function(msg) {
                console.log("Error: " + msg);
            })
            //Envoi à tous les amis
            $(friendTab).each(function (index, item){
            	data.destinataire = item.url; //Destinataire de chaque message 
            	$.ajax({
                    method: "POST",
                    url: item.url+"/api/Messages.php",
                    dataType: "text",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify(data)
                })
                .done(function(msg) {
                    //alert("Data Saved: " + msg);
                })
                .fail(function(msg) {
                    console.log("Error: " + msg);
                })
            });
            $("#bdgsucces"+clicked_id).show().delay(2000).fadeOut();
        }
    } //fin partage
</script>


</body>