<?php 
include('api/Model/DatabasesConnector.php');
include('api/Model/UserModel.php');
include('api/Model/AmisModel.php');
include('api/Model/MessagesModel.php');
include('header.php'); 

$db = new DatabaseConnector();
$UserModel = new UserModel($db->getConnection());
$User= $UserModel->getUser();
$AmisModel = new AmisModel($db->getConnection());
$Amis = $AmisModel->findFriends();
$MessagesModel = new MessagesModel($db->getConnection());
$Messages = $MessagesModel->findMineSent();
?>

<!--Mur-->
<div class="container" style="padding:10px">
    <!--Titre-->
    <h3 class="mb-5"><?php echo "Profil de : ".$User["nom"]." ".$User["prenom"]?></h3>

    <div class="row">
        <div class="col">
            <!--Liste des amis-->
            <div class="card" style="width: 18rem; position: sticky;">
              <div class="card-body">
                <h5 class="card-title">Liste d'Amis</h5>
                <ul class="list-group">
                    <!--TODO : RAJOUTER LIEN VERS PROFIL AMI-->
                    <?php foreach ($Amis as $Ami) { ?>
                        <a href="<?php echo $Ami["url"]?>/mur.php" class="list-group-item list-group-item-action"><?php echo $Ami["nom"]." ".$Ami["prenom"]?></a>
                    <?php } ?>
                </ul>
              </div>
            </div>
        </div>

        <div class="col">
        <?php foreach ($Messages as $Message) { ?>

        <div class="card-deck">
            <!--Liste de messages-->
                <div class="card shadow p-1 mb-3 bg-white rounded" style="width: 50rem;">
                    <div class="card-header"> 
                            <?php echo $Message["auteur"];
                            if ($Message["auteur"] != $Message["emetteur"])
                            echo "<span class='font-weight-bold'> >> </span>".$Message["emetteur"]."</br>"?>
                    </div>
                    <div class="card-body d-flex flex-column">
                        <p class="card-text pb-">
                            <?php echo "<span class='font-weight-bold'>Contenu : </span>".$Message["contenue"]."</br>"?>
                        </p>
                    </div>
                </div>
            </div>
             <?php } ?>
        </div>
        
                     
    </div> <!--fin row-->
</div><!-- fin conteneur-->

<script>
$(document).ready(function() {
            $("#selectFriend a").click(function(e) {
                var value = $(this).attr("friend");
                $.ajax({
                        method: "PUT",
                        url: "/api/User.php",
                        dataType: "text",
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({"value": value}),
                    })
                    .done(function() {
                        $("#modePartageVal").attr("class","modePartage"+value);
                        $("#selectFriend a").removeClass("active");
                        $("#Partage"+value).addClass("active");
                    })
                    .fail(function(msg) {
                        alert("Error: " + msg);
                    })
            });

            $("#Partage"+$("#selectFriend").attr("current")).addClass("active");
});
</script>
<?php include('footer.php'); ?>