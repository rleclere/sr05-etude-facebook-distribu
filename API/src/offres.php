<?php 
include('header.php'); 
include('api/Model/DatabasesConnector.php');
include('api/Model/OffresModel.php');
$db = new DatabaseConnector();
$OffresModel = new OffresModel($db->getConnection());
$ListeOffres = $OffresModel->findAll();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Page de offres</title>
</head>
<body>
	<h2 class="mb-4 mt-3 ml-5">Liste des annonces</h2>
	<div class="card mx-auto pb-0" style="width: 80rem">
		<form action="/api/Offres.php" method="POST">
			<label for="titre">Déposer votre annonce:</label>
			<input type="text" id="titre" name="titre" placeholder="ex: voiture">
			<input type="text" id="description" name="description" placeholder="ex: citroen c23">
			<input type="submit" value="Envoyer">
		</form>
  		<div class="card-body">
			<table class="table table-striped table-bordered mb-0">
				<thead class="thead-dark">
					<tr>
						<th>ID</th>
						<th>titre</th>
						<th>description</th>
				</thead>
				<tbody>
					<?php
					//var_dump($ListeMessages);
					$i = 0;
					foreach ($ListeOffres as $offre) {
						echo '<tr>';
						echo '<td>'.$offre["id"].'</td>';
						echo '<td>'.$offre["titre"].'</td>';
						echo '<td>'.$offre["description"].'</td>';
						echo '<td>'.'<form action="/api/Offres.php" method="POST">'.'<input type="submit" value="Supprimer" >'.'<input type="hidden" id="idToDL" name="idToDL" value='.$offre["id"].'></form>';
						echo '</tr>';
						$i++;
					}
				?>
				</tbody>
			</table>
		</div>
	</div>
</body>