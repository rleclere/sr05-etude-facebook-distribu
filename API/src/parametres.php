<?php 
include('api/Model/DatabasesConnector.php');
include('api/Model/UserModel.php');
include('header.php'); 

$db = new DatabaseConnector();
$UserModel = new UserModel($db->getConnection());
$User= $UserModel->getUser();
?>
<div class="container" style="padding:10px">


<!-- Boucle -->
<div class="card-deck" >

    <!-- Affichage offre -->
    <div class="card shadow p-1 mb-3 bg-white rounded ">
        <div class="col-lg-12 col-md-12 col-sm-8 d-flex">
            <div class="card-body d-flex flex-column">
                <p class="card-text pb-">
                    <!-- Titre -->
                    <h3 class="text-capitalize mb-5">Paramètrage du compte</h3></br>
                    <h4>Informations utilisateur</h4>
                    <div class="mb-4"><?php echo "<span class='font-weight-bold'>Nom : </span>".$User["nom"]."<span class='font-weight-bold'>    Prénom: </span>".$User["prenom"]?></div>
                    <h4>Confidentialité</h4>
                    <div><?php echo " Mode de Partage actuel : <span id='modePartageVal' class='modePartage".$User["modePartage"]."'></span>"?></div>
                </p>
                <div class="dropdown">
                  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Je partage mes informations avec...
                  </button>
                  <div class="dropdown-menu" id="selectFriend" current='<?php echo $User["modePartage"]?>' aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" id="Partage1" href="#" friend="1">Mes Amis</a>
                    <a class="dropdown-item" id="Partage2" href="#" friend="2">Mes Amis et leur Amis</a>
                    <a class="dropdown-item" id="Partage3" href="#" friend="3">Tout le monde</a>
                  </div>
                </div>
        
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
            $("#selectFriend a").click(function(e) {
                var value = $(this).attr("friend");
                $.ajax({
                        method: "PUT",
                        url: "/api/User.php",
                        dataType: "text",
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({"value": value}),
                    })
                    .done(function() {
                        $("#modePartageVal").attr("class","modePartage"+value);
                        $("#selectFriend a").removeClass("active");
                        $("#Partage"+value).addClass("active");
                    })
                    .fail(function(msg) {
                        alert("Error: " + msg);
                    })
            });

            $("#Partage"+$("#selectFriend").attr("current")).addClass("active");
});
</script>
<?php include('footer.php'); ?>