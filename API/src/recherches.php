<?php 

include('api/Model/DatabasesConnector.php');
include('api/Model/AmisModel.php');
include('api/Model/utilisateurModel.php');
include('header.php'); 

$db = new DatabaseConnector();
$AmisModel = new AmisModel($db->getConnection());
$utilisateurModel = new utilisateurModel($db->getConnection());
$Amis = $AmisModel->findAll();
$uM = $utilisateurModel->find();
?>
<h2>Rechercher une annonce</h2>
<div class="container" style="padding:10px">
    <!-- Recherche -->
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <button class="btn btn-outline-primary" type="button" id="button-search" >Rechercher</button>
        </div>
            <input type="text" class="form-control" id="product" placeholder="Produit" aria-describedby="button-search">
    </div>
    <div id="popupListAnnonce" style="display:none"></div>
</div>
<script>
$(document).ready(function() {  
    $("#button-search").click(function(){
        let Me = <?php echo json_encode($uM); ?>;
        let annonces = [];
        initDemande(Me[0]);
    });

    function rechercheProduit(Me){
        let Amis = <?php echo json_encode($Amis); ?>;
        let annonces = [];
        const demande = {
            "demandeur": Me["url"],
            "parent": Me["url"],
            "produit": $("#product").val()
        }
        $("#popupListAnnonce").empty();
        $("#popupListAnnonce").append('<h4 style="text-align:center">Liste des annonces sur : ' + $("#product").val() + ' <span class="btn btn-danger mx-auto" onclick="$(\'#popupListAnnonce\').slideUp(600)">Fermer</span></h4>');

        for(const ami of Amis){
            $.ajax({
                method: "POST",
                url: ami["url"]+"/api/Recherches.php",
                contentType: 'application/json; charset=utf-8',
                dataType: "text",
                data: JSON.stringify(demande),
                success: function(data){
                    if(data.length > 0)
                        console.log(data);
                        afficherAnnonces(JSON.parse(data));

                }
            })
        }
    }

    function initDemande(Me){
        let Amis = <?php echo json_encode($Amis); ?>;
        const demande = {
            "demandeur": Me["url"],
            "parent": Me["url"]
        }
        for(const ami of Amis){
            $.ajax({
                method: "POST",
                url: ami["url"]+"/api/Recherches.php?init=1",
                contentType: 'application/json; charset=utf-8',
                dataType: "text",
                data: JSON.stringify(demande),
            })
        }
        setTimeout(function() {
            rechercheProduit(Me);
        }, 200);
    }

    function afficherAnnonces(data){
        let keys = Object.keys(data)
        for(const key of keys) {
            ligneData = data[key]
            if($("#popupListAnnonce").html().indexOf(key) == -1) {
                bloc = ''
                bloc += '<div class="card-deck" >'
                bloc += '<div class="card shadow p-1 mb-3 bg-white rounded ">'
                bloc += '<div class="col-lg-12 col-md-12 col-sm-8 d-flex">'
                bloc += '<div class="card-body d-flex flex-column">'
                bloc += '<p class="card-text pb-">'
                bloc += '<h3>'
                bloc += key + ' : '+ligneData["prenom"] + ' ' + ligneData["nom"] +' (' + ligneData["titre"] + ')'
                bloc += '</h3>'
                bloc += ligneData["description"]
                bloc += '</p>'
                bloc += '</div>'
                bloc += '</div>'
                bloc += '</div>'
                bloc += '</div>'
                $("#popupListAnnonce").append(bloc);
            }
        }
        $("#popupListAnnonce").slideDown(600);
    }
});
</script>
<?php include('footer.php'); ?>
