# SR05 Etude Facebook distribué

Facebook distribué - Algorithmes répartis dans les environnement web

**Thème :** Algorithmes répartis dans les environnement web

Étudier un "facebook" entièrement réparti :

- Chaque participant installe une application web (en PHP par exemple) sur un serveur web quelconque.
- Via ces pages, un participant peut administrer ses "amis" en indiquant l'URL de leur propre page personnelle.
- Il peut diffuser un message vers ses amis; les pages web permettent d'émettre et recevoir des messages (requêtes sur les pages des amis).
- Un message reçu peut être considéré comme intéressant pour les amis et rediffusé.
- Un participant peut faire une recherche dans les posts de ses amis ou des amis des amis, selon configuration.
- Un ami d'un ami peut devenir ami.
- **D'autres services peuvent être imaginés tant qu'un participant reste maître de ses données et de ses contacts.** -> Proposer deux trois idée, messagerie ?

Algorithme utilisé :
- Demi vague pour le partage d'information

Réunion Préliminaire : https://docs.google.com/document/d/1QvNPSUMTfFombvgOjVSal8H4bNjdNXLseDihRJNwteI/edit?fbclid=IwAR02BHkHIP1plHl044r5-eVGRlPBXqcsQAvWv5d3dyP8jQ7KDPMZifrUIg0
