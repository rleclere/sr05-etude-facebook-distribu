Creer un dossier nginx-proxy et y placer docker-compose.yml

Installer docker -> sudo snap install docker
Crée le reseau de contenaire -> sudo docker network create nginx-proxy

Il est necessaire de demarer le proxy en premier étape, pour cela, il faut allez dans le fichier /nginx-proxy/ et executer -> sudo docker-compose up -d

Pour chaque nouveau site il faut : 

Il faut ajouter dans le fichier /etc/hosts les lignes suivante : 

127.0.0.1	site.web
127.0.0.1	site.phpmyadmin

Il faut remplacer site par le nom du site

Il faut quelque part, l'aborescence suivante qui sera partagee avec le contenaire : 
/site-data/mysql
/site-data/lamp/conf
/site-data/lamp/www/site/htdocs
/site-data/lamp/www/site/log

Idealement, il faudrait renommer site par son nom

Dans un premier temps il faut executer la commande suivante avec l'option SITE_SAMPLE="create" afin d'initialiser les fichiers
Changer les chemins de gauche (-v) afin qu'il correspond au dossier crée précédament ainsi que de remplacer tout les mots site en minuscule (dans VIRTUAL-HOST) par le nom de celui-ci
Il faudrait aussi changer le name, mais ce n'est pas essentiel

sudo docker run --privileged=true -d --expose 80 --net nginx-proxy \
-v ~/site-data/:/data/ \
-v ~/site-data/mysql:/var/lib/mysql \
-e MYSQL_PASS="admin" \
-e SITE_SAMPLE="create" \
-e VIRTUAL_HOST="site.loc,site.phpmyadmin" \
--name=siteXFirst \
lioshi/lamp:php5

Une fois que l'on a vérifier que l'acces au site (navigateur -> http://site.web en remplacant site par le nom du site), on peut stopper le contener :
sudo docker stop siteXFirst
Le nom a donner est celui fournit sur l'argument --name

Puis il faut éditer le fichier site-data/lamp/conf/httpd-conf-site.conf
Remplacer le contenu du fichier par :

<VirtualHost *:80>   
  ServerName    site.web  
  DocumentRoot  /data/lamp/www/site/htdocs  
  AddType         application/x-httpd-php .php  
  DirectoryIndex  index.php  
  CustomLog     /data/lamp/www/site/log/request.log combined  
  KeepAlive Off  
  <Directory /data/lamp/www/site/htdocs>  
    Options +FollowSymLinks +ExecCGI  
  </Directory>  
</VirtualHost>

<VirtualHost *:80>   
  ServerName    site.phpmyadmin  
  DocumentRoot  /var/www/html/phpmyadmin  
  AddType         application/x-httpd-php .php  
  DirectoryIndex  index.php  
  CustomLog     /data/lamp/www/site/log/request.log combined  
  KeepAlive Off  
  <Directory /var/www/html/phpmyadmin >  
    Options +FollowSymLinks +ExecCGI  
  </Directory>  
</VirtualHost>

Il faut remplacer site par le nom du nouveau site (lignes ServerName)
Une fois que cela est fait il suffit de relancer le serveur avec la commande suivante sans le paramtre de création
Il est necessaire de faire les meme changement qu'au dessus aux lignes -v et VIRTUAL_HOST

sudo docker run --privileged=true -d --expose 80 --net nginx-proxy \
-v ~/site-data/:/data/ \
-v ~/site-data/mysql:/var/lib/mysql \
-e MYSQL_PASS="admin" \
-e VIRTUAL_HOST="site.web,site.phpmyadmin" \
--name=siteX \
lioshi/lamp:php5

Enfin on peut check l'acces a phpmyadmin et au server web (http://site.web et http://site.phpmyadmin en remplacant site par son nom)

Si il y a une erreur forbidden, essayer sudo chmod 777 -R site-data/
Evidemment si le probleme vient de la on regardera plus tard les droits precis a mettre

En cas de probleme contacter le support a romain.leclere@etu.utc.fr reponse sous 2 jours hors week end et jours feries