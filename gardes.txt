Les emissions sont déclenché par une action d'un utilisateur sur une page
Les reception correspondent chacune à une requete sur l'API


Emission d'une annonce:
	---
	Emmission d'une annonce publique diffusé sur tout le reseau sans restriction
	Declenchée lorsqu'un utilisateur poste un message de type annonce
	---
	Ajouter l'annonce a son mur
	Envoyer l'annonce a tout ses amis


Reception d'une annonce:
	---
	Reception d'une annonce publique, elle sera affiché seulement si la configuration le permet,
	et sera retransmise aux amis dans tous les cas. Algorithme de demi-vague
	L'ami ayant transmis l'annonce n'a pas d'importance, seul l'auteur originel est sauvegardé
	Parametres :
		annonce = contenu de l'annonce
		auteur = auteur de l'annonce
	Reponse : aucune ou ack (a definir)
	---

	Si l'annonce a deja été recue : ne rien faire
	Sinon, la renvoyer a tout ses amis
	Si afficherAnnonce = vrai, ajouter l'annonce au fil d'actu


Emission d'un message:
	---
	Emission d'un poste par l'utilisateur
	Declenché lorsque l'utilisateur crée un message normal ou lorsqu'il partage un message depuis son fil d'actualité
	---

	Ajouter le message au mur
	Envoyer le message a tout ses amis


Reception d'un message:
	---
	Reception d'un message de la part d'un ami
	Il est ajouté au fil d'actualité
	Parametres:
		message = Contenu du message
		auteur = Createur du message
		emeteur = Ami ayant transmis le message
	Reponse : rien ou ack (a definir)
	---
	Si emeteur n'appartient pas a AMIS :
		Envoyer un demande de suppression d'ami a emeteur
	Sinon :
		Ajouter le message au fil d'actu


Reception d'une demande de recherche sur le mur:
	---
	Reception d'une requete specifiant une recherche sur le mur
	Cette requete peut etre refusée
	Parametres:
		authMethod : ami ou amiDamis
		lien : ami reliant l'émetteur et le demandeur dans le cas ou la methode d'authentification est amiDamis
		emeteur : utilisateur voulant acceder au mur
		keyWords : Mots specifiant la recherche (rien = tout)
	Reponse : contenu du mur ou erreur
	---

	Si identification = ami:
		Si DEMANDEUR appartient a AMIS
			Renvoyer le resultat de la recherche
		Sinon
			Renvoyer "acces refusé"
	Sinon si identification = amiDamis
		Envoyer verifierAmi à lien avec l'argument emetteur
		Si reponse = valide:
			Renvoyer le resultat de la recherche
		Sinon
			Renvoyer "acces refusé"

Reception d'une demande de verification d'ami
	---
	Requete demandant de verifier qu'un utilisateur appartient a notre liste d'amis
	Parametres:
		emeteur : utilisateur envoyant la requete
		pseudo : utilisateur à verifier
	Reponse : valide ou invalide
	---

	Si pseudo appartient a AMIS
		Renvoyer valide
	Sinon
		Renvoyer invalide


Reception d'une demande d'acces a la liste d'amis:
	---
	Requete d'acces à une liste d'amis
	Parametres: 
		emeteur : utilisateur realisant la requete
	Reponse : liste d'amis ou erreur
	---

	Si emeteur appartient a AMIS
		Renvoyer la liste
	Sinon
		Renvoyer "acces refusé"


Emission d'une suppression d'un utilisateur de la liste d'amis
	---
	Un utilisateur demande la suppression d'un autre utilisateur de sa liste d'amis
	---

	Retirer l'utilisateur des amis
	Envoi d'un message de suppression d'amis a l'utilisateur

Reception d'un message de suppression:
	---
	Un utilisateur a supprimé ce site de ses amis. Il faut le supprimer en retour
	Parametres :
		emeteur : utilisateur a supprimer
	Reponse : rien ou ack (a definir)
	---
	Si emeteur appartient a AMIS:
		Retirer A des amis

Emission d'une demande d'ajout d'amis:
	---
	Ajout d'un utilisateur en amis
	Cela est fait soit via la liste d'amis d'un amis, soit directement par une url
	---
	Envoyer une demande d'amis
	Ajouter la demande aux amis en attente


Reception d'une demande d'ajout d'amis:
	---
	Un utilisateur veut nous ajouter en amis
	Parametres:
		emeteur : emeteur du message
		nouvelAmi : pseudo du nouvel ami a ajouter
	Reponse : Rien ou ack (a definir)
	---
	Si emeteur != nouvelAmi:
		Si emeteur appartient a AMIS:
			Ajouter nouvelAmi a la liste des demande en attente de validation
	Sinon
		Ajouter nouvelAmi a la liste des demande en attente de validation


Emission d'une validation d'amis:
	---
	Un utilisateur a approuvé une demande d'amis
	---
	Ajouter l'ami
	Envoyer un message de validation d'amis


Reception d'une validation d'amis:
	---
	Reception d'une validation d'une demande d'amis
	Parametres:
		emeteur : amis confirmant la reception
	Reponse : rien ou ack (a definir)
	---
	Si emeteur appartient a DEMANDES:
		Ajouter emeteur en ami







